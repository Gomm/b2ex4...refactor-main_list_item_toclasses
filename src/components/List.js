import React from 'react'
import Item from './Item'


class List extends React.Component {

    static defaultProps = {
        data : ['item 6','item 7','item 8','item 9','item 10']
    }
    
    render(){
        
        // console.log (this.props)
            return (
                <div>

                    {
                        this.props.array.map( (ele) => <Item item =  {ele}/> )
                    }
                    
                </div>       
            )
    } 

} 
        

export default List

