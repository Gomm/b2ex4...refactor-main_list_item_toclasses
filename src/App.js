import React from 'react';
import './App.css';
import List from './components/List'

class App extends React.Component {

    render() {
    

      const categories = ['item 1','item 2','item 3','item 4','item 5']



      return (
        <div className="App">
          <header className="App-header">

            <List array = {categories} />

          </header>
        </div>
      );
    } 
}

export default App;
